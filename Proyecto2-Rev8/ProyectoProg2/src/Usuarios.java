/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Alvaro PC
 */

//creamos una clase que nos permita manejar todos los datos de cada usuario, la creamos privada por motivos de seguridad.
//por ser privada necesitamos crear getter y setters para esos datos.

public class Usuarios {
    private String Identificacion;
    private String Nombre;
    private String Apellido1;
    private String Apellido2;
    private String Correo;
    private int Telefono;
    private String Fotografia;
    private String Genero;
    private String Ubicacion;
    private String Usuario;
    private String Password;
    
    public Usuarios(){
    
        Identificacion = null;
        Nombre = null;
        Apellido1 = null;
        Apellido2 = null;
        Correo = null;
        Telefono = 0;
        Ubicacion = null;
        Genero = null;
        Fotografia = null;
        Usuario = null;
        Password = null;
    }
    
    public void setIdentificacion (String pIdentificacion){
        Identificacion = pIdentificacion;
    }
    public void setNombre (String pNombre){
        Nombre = pNombre;
    }
    public void setApellido1 (String pApellido1){
        Apellido1 = pApellido1;
    }
    public void setApellido2 (String pApellido2){
        Apellido2 = pApellido2;
    }
    public void setCorreo (String pCorreo){
        Correo = pCorreo;
    }
    public void setTelefono (int pTelefono){
        Telefono = pTelefono;
    }
    public void setUbicacion (String pUbicacion){
        Ubicacion = pUbicacion;  
    }
    public void setGenero (String pGenero){
        Genero = pGenero;
    }
    public void setFotografia (String pFotografia){
        Fotografia = pFotografia;
    }
    public void setUsuario (String pUsuario){
        Usuario = pUsuario;
    }
    public void setPassword (String pPassword){
        Password = pPassword;
    }
    public String getIdentificacion(){
        return Identificacion;
    }
    public String getNombre(){
        return Nombre;
    }
    public String getApellido1(){
        return Apellido1;
    }
    public String getApellido2(){
        return Apellido2;
    }
    public String getCorreo(){
        return Correo;
    }
    public int getTelefono(){
        return Telefono;
    }
    public String getUbicacion(){
        return Ubicacion;
    }      
    public String getGenero(){
        return Genero;
    }
    public String getFotografia(){
        return Fotografia;
    }
    public String getUsuario(){
        return Usuario;
    }
    public String getPassword(){
        return Password;
    }
}







































