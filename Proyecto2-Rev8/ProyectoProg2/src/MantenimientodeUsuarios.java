
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author Alvaro PC
 */
public class MantenimientodeUsuarios extends javax.swing.JFrame {

    /**
     * Creates new form MantenimientodeUsuarios
     */
    public MantenimientodeUsuarios() {
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        CargarDatos();
        setTitle("Mantenimiento de Usuarios");
    }

    //La funcion CargarDatos() crea una lista tipo array para los usuarios, luego se conecta a la base de datos y procede a leer los datos, luego crea una tabla
    //y agrega las columnas que queremos mostrar
    public void CargarDatos(){
        try {
        ArrayList<Usuarios> lista = new ArrayList<>();
        conexion obj = new conexion();
            try (Statement st = obj.connection()) {
                String query = "SELECT Genero, Identificacion, Nombre, Apellido1, Apellido2, telefono, ubicacion, correo FROM 'Informacion Usuarios'";
                ResultSet rs = st.executeQuery(query);
                while(rs.next()){
                    Usuarios usuario = new Usuarios();
                    usuario.setIdentificacion(rs.getString("Identificacion"));
                    usuario.setNombre(rs.getString("Nombre"));
                    usuario.setApellido1(rs.getString("Apellido1"));
                    usuario.setApellido2(rs.getString("Apellido2"));
                    usuario.setGenero(rs.getString("Genero"));
                    usuario.setTelefono(rs.getInt("telefono"));
                    usuario.setUbicacion(rs.getString("Ubicacion"));
                    usuario.setCorreo(rs.getString("correo"));
                    lista.add(usuario);
                }
                DefaultTableModel model = new DefaultTableModel();
                model.addColumn("Identificacion");
                model.addColumn("Nombre");
                model.addColumn("Primer Apellido");
                model.addColumn("Segundo Apellido");
                model.addColumn("Genero");
                model.addColumn("telefono");
                model.addColumn("Ubicacion");
                for(int x = 0; x < lista.size(); x++){
                    Usuarios e = lista.get(x);
                    model.addRow(new Object[]{e.getIdentificacion(),
                        e.getNombre(),
                        e.getApellido1(),
                        e.getApellido2(),
                        e.getGenero(),
                        e.getTelefono(),
                        e.getUbicacion()
                    });
                }
                tblUsuarios.setModel(model);
            }
            obj.connection().close();
        } catch (SQLException e) {
          String error = e.getMessage();
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnBack = new javax.swing.JButton();
        btnAgregarUsuario = new javax.swing.JButton();
        btnEditarUsuario = new javax.swing.JButton();
        btnEliminarUsuario = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblUsuarios = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/Anterior.png"))); // NOI18N
        btnBack.setBorderPainted(false);
        btnBack.setContentAreaFilled(false);
        btnBack.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/AnteriorW.png"))); // NOI18N
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        btnAgregarUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/1387940.png"))); // NOI18N
        btnAgregarUsuario.setBorderPainted(false);
        btnAgregarUsuario.setContentAreaFilled(false);
        btnAgregarUsuario.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/1387940C.png"))); // NOI18N
        btnAgregarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarUsuarioActionPerformed(evt);
            }
        });

        btnEditarUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/2226676.png"))); // NOI18N
        btnEditarUsuario.setBorderPainted(false);
        btnEditarUsuario.setContentAreaFilled(false);
        btnEditarUsuario.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/2226676C.png"))); // NOI18N
        btnEditarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarUsuarioActionPerformed(evt);
            }
        });

        btnEliminarUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/748138.png"))); // NOI18N
        btnEliminarUsuario.setBorderPainted(false);
        btnEliminarUsuario.setContentAreaFilled(false);
        btnEliminarUsuario.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imgs/748138C.png"))); // NOI18N
        btnEliminarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarUsuarioActionPerformed(evt);
            }
        });

        tblUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblUsuarios.setOpaque(false);
        tblUsuarios.setShowGrid(false);
        jScrollPane1.setViewportView(tblUsuarios);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 873, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnAgregarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(86, 86, 86)
                        .addComponent(btnEditarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(84, 84, 84)
                        .addComponent(btnEliminarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(21, 21, 21)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 314, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAgregarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEditarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEliminarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnEliminarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarUsuarioActionPerformed

        //Levantamos una ventana de dialogo que pide confirmacion que el usuario selecionado sea el correcto.
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(this, "Desea Eliminar al Usuario seleccionado?", "Eliminar Usuario", dialogButton);
        if (dialogResult == 0){
            try {
                int fila = tblUsuarios.getSelectedRow();
                String Identificacion = tblUsuarios.getValueAt(fila, 0).toString();
                conexion con = new conexion();
                try (Statement st = con.connection()) {
                    String query = "DELETE FROM 'Informacion Usuarios' WHERE Identificacion = '" + Identificacion + "' ";
                    st.executeUpdate(query);
                }
                con.connection().close();
                CargarDatos();
            } catch (SQLException e) {
            }
        }
        if (dialogResult == -1){
            this.dispose();
        }
    }//GEN-LAST:event_btnEliminarUsuarioActionPerformed

    private void btnEditarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarUsuarioActionPerformed
        
        //Levanta una ventana de dialogo que pregunta si el usuario seleccionado es el correcto.
        //De ser correcta las seleccion se levanta una nueva ventana y se manda el nombre del usuario a editar.
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(this, "Desea Editar al Usuario seleccionado?", "Editar Usuario", dialogButton);
        if (dialogResult == 0){
        int fila = tblUsuarios.getSelectedRow();
        if (fila == -1){
            JOptionPane.showMessageDialog(this, "Debe Seleccionar alguna linea");
        }
        else{
        String Identificacion = tblUsuarios.getValueAt(fila, 0).toString();
        PantallaEditarUsuario obj = new PantallaEditarUsuario();
        PantallaEditarUsuario.txtIdentificacion.setText(Identificacion);
        obj.CargarDatosEditar();
        obj.setVisible(true);
        this.dispose();
        }
        }
        if (dialogResult == -1){
            this.dispose();
        }
    }//GEN-LAST:event_btnEditarUsuarioActionPerformed

    private void btnAgregarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarUsuarioActionPerformed
       
        //Levanta una ventana de confirmacion y pregunta si se desea agregar un nuevo usuario.
        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(this, "Desea agregar un Usuario?", "Agregar Usuario", dialogButton);
        if (dialogResult == 0){
          PantallaAgregarUsuario pantalla = new PantallaAgregarUsuario();
          pantalla.setVisible(true);
          dispose();
        }
        if (dialogResult == -1){
            this.dispose();
        }
    }//GEN-LAST:event_btnAgregarUsuarioActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        
          PantallaPrincipal pantalla = new PantallaPrincipal();
          pantalla.setVisible(true);
          this.dispose();
        
    }//GEN-LAST:event_btnBackActionPerformed


    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MantenimientodeUsuarios.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MantenimientodeUsuarios.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MantenimientodeUsuarios.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MantenimientodeUsuarios.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new MantenimientodeUsuarios().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregarUsuario;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnEditarUsuario;
    private javax.swing.JButton btnEliminarUsuario;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblUsuarios;
    // End of variables declaration//GEN-END:variables
}

