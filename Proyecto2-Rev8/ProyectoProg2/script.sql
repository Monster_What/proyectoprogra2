/*
CREATE TABLE IF NOT EXISTS 'user' (
  'Identificacion' VARCHAR(12) NOT NULL,
  'Usuario' VARCHAR(16) NOT NULL,
  'email' VARCHAR(255) NOT NULL,
  'Contraseņa' VARCHAR(32) NOT NULL,
  'create_time' TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY ('Identificacion'));
*/

-- -----------------------------------------------------
-- Table 'Informacion Usuarios'
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS 'Informacion Usuarios' (
  'Genero' VARCHAR(1) NOT NULL,
  'Identificacion' VARCHAR(12) NOT NULL,
  'Nombre' VARCHAR(20) NOT NULL,
  'Apellido1' VARCHAR(20) NOT NULL,
  'Apellido2' VARCHAR(20) NOT NULL,
  'telefono' INT NOT NULL,
  'fotografia' VARCHAR(255) NOT NULL,
  'ubicacion' VARCHAR(255) NOT NULL,
  'correo' VARCHAR(255) NOT NULL,
  'Usuario' VARCHAR(255) NOT NULL,
  'Contraseņa' VARCHAR(255) NOT NULL,
  PRIMARY KEY ('Identificacion'));

insert into 'Informacion Usuarios' ("Genero", "Identificacion", "Nombre", "Apellido1", "Apellido2", "telefono", "fotografia", "ubicacion","correo","Usuario","Contraseņa")
values ("M", "01-0100-0100", "Admin", "Admin", "Admin", "00000000", "0", "1,1","admin@admin.com","Admin","admin")

/*
-- -----------------------------------------------------
-- Table 'Informacion Usuarios_has_user'
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS 'Informacion Usuarios_has_user' (
  'Informacion Usuarios_Identificacion' VARCHAR(12) NOT NULL,
  'user_Identificacion' VARCHAR(12) NOT NULL,
  PRIMARY KEY ('Informacion Usuarios_Identificacion', 'user_Identificacion'),
  CONSTRAINT 'fk_Informacion Usuarios_has_user_Informacion Usuarios'
    FOREIGN KEY ('Informacion Usuarios_Identificacion')
    REFERENCES 'Informacion Usuarios' ('Identificacion')
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT 'fk_Informacion Usuarios_has_user_user1'
    FOREIGN KEY ('user_Identificacion')
    REFERENCES 'user' ('Identificacion')
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

insert into 'user' ("Identificacion", "Usuario", "email", "Contraseņa")
values ("01-0100-0100","Admin", "admin@admin.com", "admin")
select * from user

*/
